<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'veloce');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '#/|){7D#HJLFz3az<B$>1/5aOF,Rx7H5~4SR9Lx5hqL|W[<OM0N{q[}CJJWpJOe)');
define('SECURE_AUTH_KEY',  '<w)GaK}u$+tJa5C(%>P,Wg#+7{^(G(O6:.YdCE1Rb42SSWjry[M%ta$&su+^4+TL');
define('LOGGED_IN_KEY',    'oyTjK-+4A<DJf5J<F_Z/#G^L8jyhisMk}>{<+h[e>1},#+,iitk!o;rn[w{&(9^d');
define('NONCE_KEY',        '_2bx]VTI|>W&z<t0nM)+Uu#*k=7^(Iy3**qZeQp$Nb>FzQuWF2RPTsO&kQ%{ROW,');
define('AUTH_SALT',        'T-=)Pp%X0cMwWtG3lf-10H4/DD<=$xrw$UbE-Md|&ti] -8{B:D>D5I8G1_,>DAS');
define('SECURE_AUTH_SALT', 'gqgoYd,?w|CBZ{Ou-MuK!}at=@E9+y(yy8xJU/gYC|#qbD_neyeEv^fW,+=T$a^=');
define('LOGGED_IN_SALT',   ':?Q^=37DJbbMJpT~Yz0Ago||-$;PUkq~R `Z+&LsK}@Xf1vX| Dq9spt,7Exvs#?');
define('NONCE_SALT',       ',Sv^TVk4 Dx+vKQg,qhD.^P<.1$2es0H<1G1Or*yRI.)TG.-!e4R9PGj(tc*[tLF');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
